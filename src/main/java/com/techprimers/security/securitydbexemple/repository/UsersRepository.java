package com.techprimers.security.securitydbexemple.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.techprimers.security.securitydbexemple.model.Users;

public interface UsersRepository extends JpaRepository<Users, Integer> {
	Optional<Users> findByName(String username);
}
